const assert = require('assert');

Feature('Menyukai restoran');

Before((I) => {
  I.amOnPage('/#/favorites');
});

Scenario('Tampilkan tidak ada', (I) => {
  I.see('Tidak ada data favorite', 'no-items');
});

Scenario('Menambah restoran ke daftar favorit', async (I) => {
  I.see('Tidak ada data favorite', 'no-items');
  
  I.amOnPage('/');
  
  I.seeElement('.restaurant .name a');
  
  const firstRestaurant = locate('.restaurant .name a').first();
  const firstRestaurantName = await I.grabTextFrom(firstRestaurant);
  I.click(firstRestaurant);

  I.seeElement('[aria-label="Tambahkan ke favorite"]');
  I.click('[aria-label="Tambahkan ke favorite"]');
  I.see('Sukses menyimpan favorite', '.toast');

  I.amOnPage('/#/favorites');
  I.seeElement('.restaurant .name a');
  const favoriteRestaurantName = await I.grabTextFrom('.restaurant .name a');

  assert.strictEqual(firstRestaurantName, favoriteRestaurantName);
});
