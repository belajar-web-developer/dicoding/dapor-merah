const assert = require('assert').strict;
const faker = require('faker/locale/id_ID');

Feature('Membuat Review Restoran');

Scenario('Membuat review restoran', async (I) => {
  I.amOnPage('/');
  
  I.seeElement('.restaurant .name a');

  const firstRestaurant = locate('.restaurant .name a').first();
  const firstRestaurantName = await I.grabTextFrom(firstRestaurant);
  I.click(firstRestaurant);

  I.seeElement('#formReview');
  
  const reviewerName = faker.name.findName();
  I.fillField('#inputName', reviewerName); 
  I.fillField('#inputReview', 'Masakanya sangat mewah saya suka'); 
  I.click('#formReview [type="submit"]');
  I.see('Sukses Menyimpan review', '.toast');

  I.seeElement('.review-item .name');
  const reviewerNames = await I.grabTextFrom('.review-item .name');
  reviewerNames.includes(reviewerName)
});
