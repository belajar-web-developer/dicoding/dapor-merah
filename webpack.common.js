/**
 * bersihkan folder dist
 */

const webpack = require('webpack');
const path = require('path');
const {
  CleanWebpackPlugin,
} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
/**
 * optimasi asssets
 */
const TerserJSPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ImageminWebpackPlugin = require('imagemin-webpack-plugin').default;
const ImageminMozjpeg = require('imagemin-mozjpeg');
const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin');
const sharp = require('sharp');

const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const ResourceHintWebpackPlugin = require('resource-hints-webpack-plugin');
const HTMLInlineCSSWebpackPlugin = require('html-inline-css-webpack-plugin').default;
const {
  InjectManifest,
} = require('workbox-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CONFIG_ALIAS = require('./alias.conf.js');

module.exports = {
  devServer: {
    compress: true,
    clientLogLevel: 'silent',
    stats: {
      colors: true,
      hash: false,
      version: false,
      timings: false,
      assets: false,
      chunks: false,
      modules: false,
      reasons: false,
      children: false,
      source: false,
      errors: true,
      errorDetails: true,
      warnings: true,
      publicPath: false,
    },
  },
  entry: path.resolve(__dirname, 'src/scripts/index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[hash].js',
  },
  resolve: {
    alias: CONFIG_ALIAS,
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      minSize: 20000,
      maxSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 30,
      maxInitialRequests: 30,
      automaticNameDelimiter: '~',
      enforceSizeThreshold: 50000,
      cacheGroups: {
        defaultVendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
      },
    },
    minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
  },
  module: {
    rules: [{
      test: /\.s?[ac]ss$/,
      use: [
        MiniCssExtractPlugin.loader,
        {
          loader: 'css-loader',
        },
        {
          loader: 'postcss-loader',
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
          },
        },
      ],
    },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.browser': 'true',
    }),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/templates/index.html'),
      filename: 'index.html',
      prefetch: ['**/*.*'],
      preload: ['**/*.*'],
    }),
    new ResourceHintWebpackPlugin(),
    new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: 'async',
    }),
    new HTMLInlineCSSWebpackPlugin(),
    new FaviconsWebpackPlugin({
      logo: './src/public/images/logo.png',
      cache: true,
      inject: true,
      outputPath: 'favicons',
      prefix: 'favicons',
      favicons: {
        appName: 'Dapor Merah',
        appShortName: 'DaporM',
        appDescription: 'Resto Dapur Merah',
        background: '#FFFFFF',
        theme_color: '#DC3731',
        display: 'standalone',
        start_url: '/index.html',
        orientation: 'portrait',
        loadManifestWithCredentials: true,
        icons: {
          appleStartup: false,
          yandex: false,
        },
      },
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'src/public/',
          globOptions: {
            ignore: [
              '**/images/heros/**',
            ],
          },
        },
        {
          from: 'src/public/images/heros/*.jpg',
          to: 'images/heros/[name]-small.[ext]',
          flatten: true,
          transform: (content) => sharp(content).resize(480).toBuffer()
          // return sharp(content).resize(480, jm).toBuffer()
          ,
        },
        {
          from: 'src/public/images/heros/*.jpg',
          to: 'images/heros/[name]-large.[ext]',
          flatten: true,
          transform: (content) => sharp(content).resize(800).toBuffer(),
        },
      ],
    }),
    new InjectManifest({
      swSrc: './src/scripts/sw.js',
      swDest: 'sw.js',
      include: [/\.html$/, /\.js$/, /\.css$/, /\.jpg$/, /\.jpeg$/, /\.webp$/, /\.png$/, /\.ico$/, /\.json$/],
    }),
    new ImageminWebpackPlugin({
      plugins: [
        ImageminMozjpeg({
          quality: 50,
          progressive: true,
        }),
      ],
    }),
    new ImageminWebpWebpackPlugin({
      config: [{
        test: /(^((?!favicons).)*)(\.(jpe?g|png))/,
        options: {
          quality: 50,
        },
      }],
      overrideExtension: true,
    }),
    new BundleAnalyzerPlugin(),
  ],
};
