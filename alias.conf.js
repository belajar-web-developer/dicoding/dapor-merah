const path = require('path');
const alias = {
  '@': path.resolve(__dirname, 'src/'),
  data: path.resolve(__dirname, 'src/scripts/data'),
  style: path.resolve(__dirname, 'src/styles'),
  script: path.resolve(__dirname, 'src/scripts'),
  utils: path.resolve(__dirname, 'src/scripts/utils'),
  view: path.resolve(__dirname, 'src/scripts/views'),
  config: path.resolve(__dirname, 'src/scripts/globals/config'),
  page: path.resolve(__dirname, 'src/scripts/views/pages'),
  template: path.resolve(__dirname, 'src/scripts/views/templates'),
  'url-parser': path.resolve(__dirname, 'src/scripts/routes/url-parser'),
}

module.exports = alias