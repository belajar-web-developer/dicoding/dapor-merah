import FavoriteRestaurantIdb from 'data/favorite-restaurant-idb';
import * as TestFactories from './helpers/testFactories'

describe("Batal menyukai restoran", () => {
  const addFavoriteButtonContainer = () => {
    document.body.innerHTML = '<div id="FavoriteButtonContainer"></div>';
  };

  beforeEach(async () => {
    addFavoriteButtonContainer();
    await FavoriteRestaurantIdb.putRestaurant({ id: 1 });

  });

  afterEach(async () => {
    await FavoriteRestaurantIdb.deleteRestaurant(1);
  });

  it('harus menunjukan tombol hapus favorit jika restoran sudah ada di data favorit', async () => {
    await TestFactories.createFavoriteButtonPresenterWithRestaurant({ id: 1 });
    expect(document.querySelector('[aria-label="Hapus dari favorite"]'))
      .toBeTruthy();
  });

  it('seharusnya tidak menampilkan tombol favorite jika rostoran sudah ada di data favorit sebelumnya', async () => {
    await TestFactories.createFavoriteButtonPresenterWithRestaurant({ id: 1 });

    expect(document.querySelector('[aria-label="Tambahkan ke favorite"]')).toBeFalsy();
  });

  it('harus bisa melakukan tindakan menghapus data restoran dari favorit', async () => {
    await TestFactories.createFavoriteButtonPresenterWithRestaurant({ id: 1 });

    document.querySelector('[aria-label="Hapus dari favorite"]').dispatchEvent(new Event('click'));
    expect(await FavoriteRestaurantIdb.getAllRestaurants()).toEqual([]);
  });

  it('harusnya tidak error jika mengahapus data restoran yang tidak ada di daftar favorit', async () => {
    await TestFactories.createFavoriteButtonPresenterWithRestaurant({ id: 1 });
    
    await FavoriteRestaurantIdb.deleteRestaurant(1);
    document.querySelector('[aria-label="Hapus dari favorite"]').dispatchEvent(new Event('click'));
    expect(await FavoriteRestaurantIdb.getAllRestaurants()).toEqual([]);
  });


});