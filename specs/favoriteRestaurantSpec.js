import FavoriteRestaurantIdb from 'data/favorite-restaurant-idb';
import * as TestFactories from './helpers/testFactories'

describe("Menyukai restoran", () => {
  const addFavoriteButtonContainer = () => {
    document.body.innerHTML = '<div id="FavoriteButtonContainer"></div>';
  };

  beforeEach(() => {
    addFavoriteButtonContainer();
  });

  it('harus menunjukan tombol favorit jika restoran belum menjadi favorit sebelumnya', async () => {
    await TestFactories.createFavoriteButtonPresenterWithRestaurant({ id: 1 });

    expect(document.querySelector('[aria-label="Tambahkan ke favorite"]'))
      .toBeTruthy();
  });

  it('seharusnya tidak menampilkan tombol hapus favorit jika rostoran belum menjadi favorit sebelumnya', async () => {
    await TestFactories.createFavoriteButtonPresenterWithRestaurant({ id: 1 });

    expect(document.querySelector('[aria-label="Hapus dari favorite"]')).toBeFalsy();
  });

  it('harus bisa menyukai restoran', async () => {
    await TestFactories.createFavoriteButtonPresenterWithRestaurant({ id: 1 });

    document.querySelector('#FavoriteButton').dispatchEvent(new Event('click'));
    const restaurant = await FavoriteRestaurantIdb.getRestaurant(1);

    expect(restaurant).toEqual({ id: 1 });

    FavoriteRestaurantIdb.deleteRestaurant(1);
  });

  it('harusnya tidak menambah data lagi jika restoran sudah di sukai', async () => {
    await TestFactories.createFavoriteButtonPresenterWithRestaurant({ id: 1 });
    
    await FavoriteRestaurantIdb.putRestaurant({
      id: 1
    });
    document.querySelector('#FavoriteButton').dispatchEvent(new Event('click'));
    expect(await FavoriteRestaurantIdb.getAllRestaurants()).toEqual([{ id: 1 }]);

    FavoriteRestaurantIdb.deleteRestaurant(1);
  });


  it("harusnya tidak menambahkan favorite jika tidak ada data id restoran", async () => {
    await TestFactories.createFavoriteButtonPresenterWithRestaurant({});
    document.querySelector('#FavoriteButton').dispatchEvent(new Event('click'));
    expect(await FavoriteRestaurantIdb.getAllRestaurants()).toEqual([]);
  });
});