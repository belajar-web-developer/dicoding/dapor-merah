const StikyBarInitiator = {
  init() {
    window.onscroll = () => { this._manipulateStiky(); };
  },
  _manipulateStiky() {
    const header = document.querySelector('header');
    const sticky = header.offsetHeight;
    if (window.pageYOffset >= sticky) {
      header.classList.add('sticky');
    } else {
      header.classList.remove('sticky');
    }
  },
};

export default StikyBarInitiator;
