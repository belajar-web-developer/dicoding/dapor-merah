const swRegister = {
  async init() {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/sw.js').then(() => {
        console.log('SW registered');
      }).catch((registrationError) => {
        console.log('SW registration failed: ', registrationError);
      });
    }
  },
};

export default swRegister;
