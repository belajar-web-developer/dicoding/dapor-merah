import { createFavoriteButtonTemplate, createFavoritedButtonTemplate } from 'template/template-creator';
import Toast from 'utils/toast';

const FavoriteButtonPresenter = {
  async init({ favoriteButtonContainer, favoriteRestaurants, restaurant }) {
    this._favoriteButtonContainer = favoriteButtonContainer;
    this._restaurant = restaurant;
    this._favoriteRestaurants = favoriteRestaurants;
    await this._renderButton();
  },

  async _renderButton() {
    const { id } = this._restaurant;

    if (await this._isRestaurantExist(id)) {
      this._renderLiked();
    } else {
      this._renderLike();
    }
  },

  async _isRestaurantExist(id) {
    const restaurant = await this._favoriteRestaurants.getRestaurant(id); return !!restaurant;
  },

  _renderLike() {
    this._favoriteButtonContainer.innerHTML = createFavoriteButtonTemplate();

    const FavoriteButton = document.querySelector('#FavoriteButton');
    FavoriteButton.addEventListener('click', async () => {
      this._favoriteRestaurants.putRestaurant(this._restaurant)
        .then(() => {
          Toast.show({
            message: 'Sukses menyimpan favorite',
            caption: 'Terimakasih telah menjadi pelanggan kami..',
            type: 'success',
          });
          this._renderButton();
        })
        .catch((error) => {
          window.console.log(error);
          Toast.show({
            message: 'Gagal menyimpan favorite',
            type: 'error',
          });
        });
    });
  },

  _renderLiked() {
    this._favoriteButtonContainer.innerHTML = createFavoritedButtonTemplate();

    const FavoriteButton = document.querySelector('#FavoriteButton');
    FavoriteButton.addEventListener('click', async () => {
      this._favoriteRestaurants.deleteRestaurant(this._restaurant.id)
        .then(() => {
          Toast.show({
            message: 'Sukses mengahapus favorite',
            type: 'success',
          });
          this._renderButton();
        })
        .catch((error) => {
          window.console.log(error);
          Toast.show({
            message: 'Gagal mengahapus favorite',
            type: 'error',
          });
        });
    });
  },
};

export default FavoriteButtonPresenter;
