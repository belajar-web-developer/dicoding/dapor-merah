const Toast = {
  show({ message, caption, type }) {
    const toastContainer = document.createElement('div');
    const toastId = `toast_${Date.now()}`;
    toastContainer.innerHTML = `
    <div class="toast ${type}" id="${toastId}">
      <div>${message}</div>
      ${caption ? `<div><small>${caption}</small></div>` : ''}
    </div>
    `;
    document.querySelector('body').appendChild(toastContainer);

    setTimeout(() => {
      toastContainer.remove();
    }, 5000);
  },
};
export default Toast;
