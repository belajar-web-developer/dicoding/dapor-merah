let _slideIndex = 1;
const HeroInitiator = {
  init({ container, heros }) {
    this._container = container;
    heros.forEach((hero) => this._createHeroItem(hero));
    this._initSlide();
  },

  _createHeroItem(hero) {
    this._container.innerHTML += `
      <div class="hero-item">
          <picture class="hero-bg">
             <source type="image/webp" data-srcset="./images/heros/${hero.image}-small.webp 480w, ./images/heros/${hero.image}-large.webp 800w" sizes="(max-width: 600px) 480px, 800px">
             <source type="image/jpeg" data-srcset="./images/heros/${hero.image}-small.jpg 480w, ./images/heros/${hero.image}-large.jpg 800w" sizes="(max-width: 600px) 480px, 800px">
             <img class="hero-bg lazyload" data-srcset="./images/heros/${hero.image}-small.jpg 480w, ./images/heros/${hero.image}-large.jpg 800w" alt="${hero.title}" sizes="(max-width: 600px) 480px, 800px">
          </picture>
          <div class="hero-content">
              <span class="hero-title">${hero.title}</span>
              <p class="hero-tagline">${hero.tagline}</p>
          </div>
      </div>
    `;
  },
  _initSlide() {
    const slides = document.getElementsByClassName('hero-item');
    for (let i = 0; i < slides.length; i += 1) {
      if (i === (_slideIndex - 1)) {
        slides[i].style.display = 'block';
      } else {
        slides[i].style.display = 'none';
      }
    }
    _slideIndex += 1;
    if (_slideIndex > slides.length) { _slideIndex = 1; }
    setInterval(this._initSlide, 5000);
  },
};

export default HeroInitiator;
