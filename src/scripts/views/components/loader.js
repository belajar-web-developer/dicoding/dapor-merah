class Loading extends HTMLElement {
  render() {
    this.innerHTML = `
      <div class="loader-container">
        <div>
          <div class="loader"></div>
        </div>
        <div class="text">Sedang memuat data ..</div>
      </div>
    `;
  }

  connectedCallback() {
    this.render();
  }
}
customElements.define('content-loader', Loading);
