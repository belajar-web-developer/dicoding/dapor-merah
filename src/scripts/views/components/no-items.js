class NoItems extends HTMLElement {
  render() {
    const text = this.getAttribute('text') ? this.getAttribute('text') : 'Tidak ada item tersedia';

    this.innerHTML = `
      <div class="no-items-container">
        <div>
          <div><img src="./images/logo-grey.png" /></div>
        </div>
        <div class="text">${text}</div>
      </div>
    `;
  }

  connectedCallback() {
    this.render();
  }
}
customElements.define('no-items', NoItems);
