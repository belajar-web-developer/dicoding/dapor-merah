import ArrayPaginate from 'paginate-array.js';
import {
  createRestaurantItemTemplate,
  createLoadMoreButtonTemplate,
  createSkeletonRestaurantItemTemplate,
} from 'template/template-creator';
import FavoriteRestaurantIdb from 'data/favorite-restaurant-idb';

const ListRestaurants = {
  render() {
    return `
      <section class="content"  id="maincontent">
          <div class="section-header">
              <h2 class="section-title">Restoran Favorite ..</h2>
          </div>
          <div id="restaurants">
            <content-loader />
          </div>
          <div class="section-footer"></div>
      </section>
    `;
  },
  async afterRender() {
    this._container = document.querySelector('#restaurants');
    this._createSkeletonLoader();
    FavoriteRestaurantIdb.getAllRestaurants()
      .then((restaurants) => {
        if (restaurants.length === 0) {
          this._container.innerHTML = '<no-items text="Tidak ada data favorite" />';
          this._container.classList.remove('card-grid');
        } else {
          this._container.innerHTML = '';
          this._container.classList.add('card-grid');
          this._restaurants = new ArrayPaginate(restaurants, 6);
          this._createRestaurantsData();
        }
      })
      .catch((error) => {
        window.console.log(error);
        this._container.innerHTML = '<no-items text="Terjadi kesalahan dalam mengambil data" />';
        this._container.classList.remove('card-grid');
      });
  },
  _createSkeletonLoader() {
    for (let i = 0; i < 6; i += 1) {
      this._container.innerHTML += createSkeletonRestaurantItemTemplate();
    }
  },
  _createRestaurantsData() {
    this._restaurants.next().forEach((restaurant) => {
      this._container.innerHTML += createRestaurantItemTemplate(restaurant);
    });
    this._createLoadMore();
  },
  _createLoadMore() {
    const sectionFooter = document.querySelector('.section-footer');
    let buttonLoadMoreComponent = document.querySelector('#buttonLoadMore');
    // cek jika paging data bisa di next
    if (this._restaurants.hasNext()) {
      if (!buttonLoadMoreComponent) {
        sectionFooter.innerHTML += createLoadMoreButtonTemplate();
        buttonLoadMoreComponent = document.querySelector('#buttonLoadMore');
        if ((buttonLoadMoreComponent) !== null) {
          buttonLoadMoreComponent.addEventListener('click', () => {
            this._createRestaurantsData();
          });
        }
      }
    } else if (!this._restaurants.hasNext() && (buttonLoadMoreComponent !== null)) {
      buttonLoadMoreComponent.remove();
    }
  },
};

export default ListRestaurants;
