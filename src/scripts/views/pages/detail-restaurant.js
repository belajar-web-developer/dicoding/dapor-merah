import UrlParser from 'url-parser';
import RestaurantSource from 'data/restaurant-source';
import {
  createRestaurantDetailtemplate,
  generateReviewItemsTemplate,
  createSkeletonRestaurantDetailtemplate,
} from 'template/template-creator';
import Toast from 'utils/toast';
import FavoriteButtonPresenter from 'utils/favorite-button-presenter';
import FavoriteRestaurantIdb from 'data/favorite-restaurant-idb';

const DetailRestaurant = {
  render() {
    return `
      <section id="restaurantContainer"></section>
      <div id="favoriteButtonContainer"></div>
    `;
  },
  async afterRender() {
    this._restaurantContainer = document.querySelector('#restaurantContainer');
    this._createSkeletonLoader();

    const url = UrlParser.parseActiveUrlWithoutCombiner();
    this._id = url.id;
    const response = await RestaurantSource.detailRestaurant(this._id);
    this._restaurantContainer.innerHTML = '';
    if (response.error) {
      this._restaurantContainer.innerHTML = `<no-items text="${response.error ? 'Terjadi kesalahan dalam mengambil data' : 'Tidak ada data tersedia'}" />`;
    } else {
      this._restaurantContainer.innerHTML = createRestaurantDetailtemplate(response.restaurant);
      const formReview = document.querySelector('#formReview');
      formReview.addEventListener('submit', (event) => {
        event.preventDefault();
        this._submitReview();
      });

      FavoriteButtonPresenter.init({
        favoriteButtonContainer: document.querySelector('#favoriteButtonContainer'),
        favoriteRestaurants: FavoriteRestaurantIdb,
        restaurant: response.restaurant,
      });
    }
  },
  _createSkeletonLoader() {
    this._restaurantContainer.innerHTML = createSkeletonRestaurantDetailtemplate();
  },
  async _submitReview() {
    const data = {
      id: this._id,
      name: document.querySelector('#formReview #inputName').value,
      review: document.querySelector('#formReview #inputReview').value,
    };
    RestaurantSource.submitReview(data)
      .then((response) => {
        Toast.show({
          type: response.error ? 'error' : 'success',
          message: response.error ? 'Gagal menyimpan review :(' : 'Sukses Menyimpan review',
          caption: response.error ? '' : 'Terimakasih ..',
        });
        if (!response.error) {
          generateReviewItemsTemplate(response.customerReviews);
          document.querySelector('#formReview').reset();
        }
      })
      .catch((error) => {
        window.console.log(error);
        Toast.show({
          type: 'error',
          message: error.message,
        });
      });
  },
};
export default DetailRestaurant;
