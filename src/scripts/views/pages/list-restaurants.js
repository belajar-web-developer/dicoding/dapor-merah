import UrlParser from 'url-parser';
import Hero from 'utils/hero-initiator';
import HerosData from 'data/hero-data';
import RestaurantSource from 'data/restaurant-source';
import ArrayPaginate from 'paginate-array.js';
import {
  createRestaurantItemTemplate,
  createLoadMoreButtonTemplate,
  createSkeletonRestaurantItemTemplate,
} from 'template/template-creator';

const ListRestaurants = {
  render() {
    this._activeUrl = UrlParser.parseActiveUrlWithCombiner();
    return `
      ${this._activeUrl !== '/' ? '' : '<section class="hero" id="hero-container"></section>'}
      <section class="content"  id="maincontent">
          <div class="section-header">
              <h2 class="section-title">Jelajahi Resto Kami ..</h2>
          </div>
          <div id="restaurants" class="card-grid">
          </div>
          <div class="section-footer"></div>
      </section>
    `;
  },
  async afterRender() {
    this._container = document.querySelector('#restaurants');
    if (this._activeUrl === '/') {
      Hero.init({
        container: document.querySelector('#hero-container'),
        heros: HerosData,
      });
    }

    this._createSkeletonLoader();
    RestaurantSource.allRestaurant()
      .then((response) => {
        if (response.error || response.count === 0) {
          this._container.innerHTML = `<no-items text="${response.error ? 'Terjadi kesalahan dalam mengambil data' : 'Tidak ada data tersedia'}" />`;
          this._container.classList.remove('card-grid');
        } else {
          const { restaurants } = response;
          this._container.innerHTML = '';
          this._restaurants = new ArrayPaginate(restaurants, 6);
          this._createRestaurantsData();
        }
      })
      .catch((error) => {
        window.console.log(error);
        this._container.classList.remove('card-grid');
        this._container.innerHTML = '<no-items text="Kesalahan dalam mengambil data" />';
      });
  },
  _createSkeletonLoader() {
    for (let i = 0; i < 6; i += 1) {
      this._container.innerHTML += createSkeletonRestaurantItemTemplate();
    }
  },
  _createRestaurantsData() {
    this._restaurants.next().forEach((restaurant) => {
      this._container.innerHTML += createRestaurantItemTemplate(restaurant);
    });
    this._createLoadMore();
  },
  _createLoadMore() {
    const sectionFooter = document.querySelector('.section-footer');
    let buttonLoadMoreComponent = document.querySelector('#buttonLoadMore');
    // cek jika paging data bisa di next
    if (this._restaurants.hasNext()) {
      if (!buttonLoadMoreComponent) {
        sectionFooter.innerHTML += createLoadMoreButtonTemplate();
        buttonLoadMoreComponent = document.querySelector('#buttonLoadMore');
        if ((buttonLoadMoreComponent) !== null) {
          buttonLoadMoreComponent.addEventListener('click', () => {
            this._createRestaurantsData();
          });
        }
      }
    } else if (!this._restaurants.hasNext() && (buttonLoadMoreComponent !== null)) {
      buttonLoadMoreComponent.remove();
    }
  },
};

export default ListRestaurants;
