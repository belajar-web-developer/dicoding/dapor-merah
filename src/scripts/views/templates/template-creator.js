import CONFIG from 'config';

const createFormReviewTemplate = () => `
<form id="formReview">
  <div class="form-group">
    <label for="inputName">Nama</label>
    <input class="form-input" name="name" id="inputName" required="required">
  </div>
  <div class="form-group">
    <label for="inputReview">Pesan Ulasan</label>
    <textarea rows="4" class="form-input" name="review" id="inputReview"  required="required"></textarea>
  </div>
  <div class="form-group">
    <button type="submit" class="btn primary" aria-label="Kirim review"><i class="material-icons" aria-hidden="true">send</i> <span>Kirim Review</span></button>
  </div>
</form>
`;

const createReviewItemTemplate = (review) => `
<div class="review-item">
  <div>
    <div class="avatar">
      <picture>
          <source type="image/webp" data-srcset="./images/avatar.webp">
          <source type="image/png" data-srcset="./images/avatar.png">
          <img class="lazyload" data-src="./images/avatar.png" alt="Avatar ${review.name}" />
      </picture>

    </div>
  </div>
  <div class="content">
    <div class="name">${review.name}</div>
    <div class="date">${review.date}</div>
    <div class="message">${review.review}</div>
  </div>
</div>
`;

const createSkeletonReviewItemTemplate = () => `
<div class="review-item">
  <div>
    <div class="avatar">
      <div class="loading" style="height: 100px"></div>
    </div>
  </div>
  <div class="content gutter-sm">
    <div class="name loading"></div>
    <div class="message loading"></div>
  </div>
</div>
`;

const createListmenu = (menu) => `
<li class="list-item">
  <span> ${menu.name} </span>
  <i class="material-icons" aria-hidden="true">chevron_right</i>
</li>
`;

const createRestaurantDetailtemplate = (restaurant) => `
<div  class="detail-gird">
  <div id="detail" class="card detail-content">
    <div class="image">
 
      <img class="lazyload"
        data-srcset="
          ${CONFIG.BASE_IMAGE_URL('small')}${restaurant.pictureId} 360w,
          ${CONFIG.BASE_IMAGE_URL('medium')}${restaurant.pictureId} 720w,
          ${CONFIG.BASE_IMAGE_URL('large')}${restaurant.pictureId} 1080w"
        
        data-src="${CONFIG.BASE_IMAGE_URL('large')}${restaurant.pictureId}"
        alt="${restaurant.name}"
      />
 
      <div class="city">${restaurant.city}</div>
    </div>
    <div class="content">
      <div class="rating">Rating: ${restaurant.rating}</div>
      <div class="name">
        ${restaurant.name}
      </div>
      <div>
        ${restaurant.categories.map((category) => `<span class="category">${category.name}</span>`).join('')}
      </div>
      <div class="address">
        ${restaurant.address}
      </div>
      <div class="description">${restaurant.description}</div>
    </div>
  </div>
  <div id="foods-menu" class="card restaurant-menu">
    <ul class="list-group">
      <li class="list-header"><h2 class="heading">Menu Makanan</h2></li>
      ${restaurant.menus.foods.map((food) => createListmenu(food)).join('')}
    </ul>
  </div>

  <div id="drink-menu" class="card drinks-menu">
    <ul class="list-group">
      <li class="list-header"><h2 class="heading">Menu Minuman</h2></li>
      ${restaurant.menus.drinks.map((drink) => createListmenu(drink)).join('')}
    </ul>
  </div>
  <div id="review" class="card">
    <div class="header">
      <h2 class="heading">Ulasan Pengunjung ..</h2>
    </div>
    <div class="reviews" id="reviewsContainer">
      ${restaurant.consumerReviews.map((review) => createReviewItemTemplate(review)).join('')}
    </div>
  </div>
  <div id="reviewFrom" class="card">
    <div class="header">
      <h2 class="heading">Berikan Ulasan</h2>
    </div>
    <div class="content">
      ${createFormReviewTemplate()}
    </div>
  </div>
</div>
`;

const createSkeletonRestaurantDetailtemplate = () => `
<div  class="detail-gird">
  <div id="detail" class="card detail-content">
    <div class="image">
      <div class="loading" style="min-height: 500px"></div>
    </div>
    <div class="content gutter-sm">
      <div class="rating loading"></div>
      <div class="name loading"></div>
      <div>
        ${[...Array(3).keys()].map(() => '<span class="category" style="width: 50px;"></span>').join('')}
      </div>
      <div class="address loading"></div>
      <div class="description loading" style="min-height: 100px"></div>
    </div>
  </div>
  <div id="foods-menu" class="card restaurant-menu">
    <ul class="list-group">
      <li class="list-header"><h2 class="heading loading"></h2></li>
      ${[...Array(4).keys()].map(() => '<li class="list-item"><div class="loading" style="width: 100%;"></div></li>').join('')}
    </ul>
  </div>

  <div id="drink-menu" class="card drinks-menu">
    <ul class="list-group">
      <li class="list-header"><h2 class="heading loading"></h2></li>
      ${[...Array(4).keys()].map(() => '<li class="list-item"><div class="loading" style="width: 100%;"></div></li>').join('')}
    </ul>
  </div>
  <div id="review" class="card">
    <div class="header">
      <h2 class="heading loading"></h2>
    </div>
    <div class="reviews" id="reviewsContainer">
      ${[...Array(2).keys()].map(() => createSkeletonReviewItemTemplate()).join('')}
    </div>
  </div>
  <div id="reviewFrom" class="card">
    <div class="header">
      <h2 class="heading loading"></h2>
    </div>
    <div class="content gutter-sm">
      <div class="loading"></div>
      <div class="loading" style="height: 50px"></div>
    </div>
  </div>
</div>
`;

const generateReviewItemsTemplate = (reviews) => {
  const container = document.querySelector('#reviewsContainer');
  container.innerHTML = reviews.map((review) => createReviewItemTemplate(review)).join('');
};

const createRestaurantItemTemplate = (restaurant) => `
  <div class="card restaurant">
    <div class="image">
      <a href="/#/detail/${restaurant.id}">

        <img class="lazyload"
          data-srcset="
            ${CONFIG.BASE_IMAGE_URL('small')}${restaurant.pictureId} 360w,
            ${CONFIG.BASE_IMAGE_URL('medium')}${restaurant.pictureId} 720w,
            ${CONFIG.BASE_IMAGE_URL('large')}${restaurant.pictureId} 1080w"
          
          data-src="${CONFIG.BASE_IMAGE_URL('large')}${restaurant.pictureId}"
          alt="${restaurant.name}"
        />

      </a>
      <div class="city">${restaurant.city}</div>
    </div>
    <div class="content">
      <div class="rating">Rating : ${restaurant.rating}</div>
      <div class="name">
        <a href="/#/detail/${restaurant.id}">
          ${restaurant.name}
        </a>
      </div>
      <div class="sort-description">${restaurant.description.substr(0, 100)} ...</div>
    </div>
  </div>
  `;

const createSkeletonRestaurantItemTemplate = () => `
  <div class="card restaurant">
    <div class="image">
      <div class="loading" style="min-height: 500px"></div>
    </div>
    <div class="content gutter-sm">
      <div class="rating loading"></div>
      <div class="name loading"></div>
      <div class="sort-description loading"></div>
    </div>
  </div>
  `;

const createFavoriteButtonTemplate = () => `
  <button aria-label="Tambahkan ke favorite" id="FavoriteButton" class="fab primary">
    <i class="material-icons" aria-hidden="true">favorite_border</i>
  </button>
`;

const createFavoritedButtonTemplate = () => `
  <button aria-label="Hapus dari favorite" id="FavoriteButton" class="fab primary">
    <i class="material-icons" aria-hidden="true">favorite</i>
  </button>
`;

const createLoadMoreButtonTemplate = () => `
  <button type="button" class="btn load-more" aria-label="Muat lebih banyak" id="buttonLoadMore">
    <i class="material-icons" aria-hidden="true">refresh</i> <span>Muat Lebih Banyak</span>
  </button>
`;

export {
  createRestaurantItemTemplate,
  createLoadMoreButtonTemplate,
  createRestaurantDetailtemplate,
  createFavoriteButtonTemplate,
  createFavoritedButtonTemplate,
  generateReviewItemsTemplate,
  createSkeletonRestaurantItemTemplate,
  createSkeletonRestaurantDetailtemplate,
};
