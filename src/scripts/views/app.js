import DrawerInitiator from 'utils/drawer-initiator';
import StikyBarInitiator from 'utils/stiky-appbar-initiator';
import UrlParser from 'script/routes/url-parser';
import routes from 'script/routes/routes';
import 'view/components';

class App {
  constructor({ button, drawer, content }) {
    this._button = button;
    this._drawer = drawer;
    this._content = content;
    this._initialAppShell();
  }

  _initialAppShell() {
    DrawerInitiator.init({
      button: this._button,
      drawer: this._drawer,
      content: this._content,
    });
    StikyBarInitiator.init();
  }

  async renderPage() {
    const url = UrlParser.parseActiveUrlWithCombiner();
    const page = routes[url];
    this._content.innerHTML = await page.render();
    await page.afterRender();
  }
}

export default App;
