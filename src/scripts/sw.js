import 'regenerator-runtime';

import { setCacheNameDetails } from 'workbox-core';
import { precacheAndRoute } from 'workbox-precaching';
import { registerRoute } from 'workbox-routing';
import { StaleWhileRevalidate, CacheFirst } from 'workbox-strategies';
import { CacheableResponse } from 'workbox-cacheable-response';
import { ExpirationPlugin } from 'workbox-expiration';
import CONFIG from 'config';

setCacheNameDetails({
  prefix: CONFIG.CACHE_NAME,
  suffix: 'v1',
  precache: 'precache',
  runtime: 'runtime',
  googleAnalytics: 'google-analytics',
});

precacheAndRoute([
  ...self.__WB_MANIFEST,
  { url: '/images/heros/hero-image_1-small.webp', revision: null },
  { url: '/images/heros/hero-image_1-large.webp', revision: null },
  { url: '/images/heros/hero-image_2-small.webp', revision: null },
  { url: '/images/heros/hero-image_2-large.webp', revision: null },
  { url: '/images/heros/hero-image_4-small.webp', revision: null },
  { url: '/images/heros/hero-image_4-large.webp', revision: null },
]);

registerRoute(
  new RegExp(CONFIG.BASE_URL),
  new StaleWhileRevalidate({
    cacheName: 'api-cache',
  }),
);

registerRoute(
  /^https:\/\/fonts\.googleapis\.com/,
  new StaleWhileRevalidate({
    cacheName: 'google-fonts-stylesheets',
  }),
);

registerRoute(
  /^https:\/\/fonts\.gstatic\.com/,
  new CacheFirst({
    cacheName: 'google-fonts-webfonts',
    plugins: [
      new CacheableResponse({
        statuses: [0, 200],
      }),
      new ExpirationPlugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
        maxEntries: 30,
      }),
    ],
  }),
);

self.addEventListener('push', (event) => {
  let body;
  if (event.data) {
    body = event.data.text();
  } else {
    body = 'Push message no payload';
  }
  const options = {
    body,
    icon: 'images/logo.png',
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: 1,
    },
  };
  event.waitUntil(
    self.registration.showNotification('Push Notification', options),
  );
});
