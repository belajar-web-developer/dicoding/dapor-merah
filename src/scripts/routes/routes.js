import ListRestaurants from 'page/list-restaurants';
import DetailRestaurant from 'page/detail-restaurant';
import Favorite from 'page/favorite-restaurants';

const routes = {
  '/': ListRestaurants, // default page
  '/list-restaurants': ListRestaurants,
  '/detail/:id': DetailRestaurant,
  '/favorites': Favorite,
};

export default routes;
