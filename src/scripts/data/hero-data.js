export default [
  {
    title: 'Masakan mewah ala restorant bintang 5',
    image: 'hero-image_4',
    tagline: 'Jadilah yang pertama dari teman temanmu mencicipi inovasi masakan terbaru dari kami',
  }, {
    title: 'Nikmati masakan dari koki internasional',
    image: 'hero-image_1',
    tagline: 'Kami selalu mengedepankan kualitas kezelatan di setiap makanan, dengan mengadirkan koki internasional',
  }, {
    title: 'Beragam Inovasi masakan baru',
    image: 'hero-image_2',
    tagline: 'Selalu nikmati hidangan hidangan populer yang kami sajikan',
  },
];
