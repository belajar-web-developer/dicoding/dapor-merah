const CONFIG = {
  APP_NAME: 'DaporMerah',
  APP_FULL_NAM: 'Dapor Merah',
  KEY: '12345',
  BASE_URL: 'https://dicoding-restaurant-api.el.r.appspot.com/',
  BASE_IMAGE_URL: (type = 'medium') => `https://dicoding-restaurant-api.el.r.appspot.com/images/${type}/`,
  DEFAULT_LANGUAGE: 'en-us',
  CACHE_NAME: 'dapor-merah',
  DATABASE_NAME: 'dapor-merah-database',
  DATABASE_VERSION: 1,
  OBJECT_STORE_NAME: 'restaurants',
};

export default CONFIG;
